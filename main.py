import pandas as pandas
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from slugify import slugify


def main():
    # Faz a leitura dos arquivos
    dfPacientes, dfDesfechos, dfExames = LeDados("HSL_Pacientes_3.csv", "HSL_Desfechos_3.csv", "HSL_Exames_3.csv")
    #dfPaciente   = ReadPacientes('HSL_Pacientes_3.csv')
    #dfResultados = ReadResultados('HSL_Desfechos_3.csv')
    #dfExames = ReadExames('HSL_Exames_3.csv')

    # Faz o Joinda tabela paciente com os atendimentos.
    #dfPR = JoinPacienteResultados(dfPaciente, dfResultados, True)
    #CalcExames(dfPR)
    #dfPR, dfExames = ReadPRExames()
    #JoinPRExames(dfPR, dfExames)
    #CalcExames()

def ReadPRExames():
    exames  = pandas.read_csv("exames.csv",delimiter=";" )
    pr      = pandas.read_csv("paciente_desfecho.csv",delimiter=";" )
    return pr, exames


def JoinPacienteResultados(dfPaciente, dfResultados, save= False):
    df = dfPaciente.set_index('ID_PACIENTE').join(dfResultados.set_index('ID_PACIENTE'))
    df = df.dropna()
    #print(df.describe(include='all', percentiles=[.25, .5, .75]))
    df['DAYS'] = (df['DT_DESFECHO'] - df['DT_ATENDIMENTO']).dt.days
    df = df.query('DAYS > 1 and aa_nascimento > 1971 and aa_nascimento < 2001')
    print(df.describe(include='all', percentiles=[.25, .5, .75]))
    if save:
        df.to_csv('paciente_desfecho.csv',  index=True, sep=";")
        df.groupby('ID_PACIENTE').size().to_csv('numeroatendimentos.csv',  index=True, sep=";")

    return df

def Graph(dfPR):
    print(dfPR.describe(include='all', percentiles=[.25, .5, .75]))
    #fig = plt.figure(figsize = (8,8))
    #ax = fig.gca()
    #df.hist(ax=ax, bins=10)
    #dfDays = df.filter(items=['DAYS', 'IC_SEXO']) 
    #df.boxplot(column=['DAYS'], by='IC_SEXO')
    #dfDays.hist(ax=ax, bins=15)
    #plt.title("Boxplot por dias internado agrupado por sexo")
    #plt.show()

    #df.boxplot(column=['aa_nascimento'], by='IC_SEXO')
    #dfDays.hist(ax=ax, bins=15)
    #plt.title("Boxplot ano de nascimento por sexo")
    #plt.show()

    #fig = plt.figure(figsize = (8,8))
    #ax2 = fig.gca()
    #df.hist(ax=ax2, bins=15, column=['DAYS'])
    #plt.title("Histograma de Dias")
    #plt.show()

    #fig = plt.figure(figsize = (8,8))
    #ax2 = fig.gca()
    #df.hist(ax=ax2, bins=15, column=['aa_nascimento'])
    #plt.title("Histograma de aa_nascimento")
    #plt.show()

# Le arquivo de pacientes
# Remove colunas desnecessárias (CD_PAIS, CD_UF, CD_MUNICIPIO, CD_CEPREDUZIDO)
# Remove pacinetes com ano de nascimento inválidos
# Converte ano de nascimento em numérico.
# Salva em CSV
def ReadPacientes(filename, save=False):
    pacientes  = pandas.read_csv(filename,delimiter="|" )
    dfPaciente   = pandas.DataFrame(pacientes, columns= ['ID_PACIENTE','IC_SEXO', 'aa_nascimento'])
    dfPaciente   = dfPaciente.query('aa_nascimento != "YYYY"')
    dfPaciente   = dfPaciente.query('aa_nascimento != "AAAA"')
    dfPaciente.loc[:,'aa_nascimento'] = pandas.to_numeric(dfPaciente.loc[:,'aa_nascimento'], errors='ignore')
    print(dfPaciente)
    if (save):
        dfPaciente.to_csv("pacientes.csv", index=False, sep=";", encoding='utf-8')

    return dfPaciente

# Le arquivo de desfechos
# Remove colunas desnecessárias
# Converte colunas com data em tipo datetime
# Normaliza strings
# Salva em CSV
def ReadDesfechos(filename, save=True):
    resultados = pandas.read_csv(filename, delimiter="|", encoding='utf-8', engine='python')
    dfResultados = pandas.DataFrame(resultados, columns=['ID_PACIENTE', 'ID_ATENDIMENTO', 'DT_ATENDIMENTO', 'DT_DESFECHO', 'DE_DESFECHO' ])
    dfResultados['DT_ATENDIMENTO'] = pandas.to_datetime(dfResultados['DT_ATENDIMENTO'], dayfirst=True)
    dfResultados['DT_DESFECHO'] = pandas.to_datetime(dfResultados['DT_DESFECHO'], dayfirst=True, errors='coerce', format="%d/%m/%Y")
    dfResultados['DE_DESFECHO'] = dfResultados['DE_DESFECHO'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')

    if (save):
        dfResultados.to_csv("resultados.csv", index=False, sep=";", encoding='utf-8')

    return dfResultados

# Le arquivo de exames
# Converte colunas com data em tipo datatime
# Normaliza strings
# Salva em CSV
def ReadExames(filename, save=True):
    exames   = pandas.read_csv(filename, delimiter="|" )
    dfExames = pandas.DataFrame(exames)

    dfExames['DT_COLETA'] = pandas.to_datetime(dfExames['DT_COLETA'], dayfirst=True)
    dfExames['DE_RESULTADO'] = dfExames['DE_RESULTADO'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
    dfExames['DE_RESULTADO'] = dfExames['DE_RESULTADO'].str.replace(',', '.')
    dfExames['DE_RESULTADO'] = pandas.to_numeric(dfExames['DE_RESULTADO'], errors="ignore")
    dfExames['DE_VALOR_REFERENCIA'] = dfExames['DE_VALOR_REFERENCIA'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
    dfExames['DE_ORIGEM'] = dfExames['DE_ORIGEM'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
    dfExames['DE_EXAME'] = dfExames['DE_EXAME'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')
    dfExames['DE_ANALITO'] = dfExames['DE_ANALITO'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')

    if(save):
        dfExames.to_csv("exames.csv", index=False, sep=";", encoding='utf-8') 

    return dfExames

# Le arquivos de dados
def LeDados(filepacientes, filedesdechos, fileexames, save=True):
    dfPacientes = ReadPacientes(filepacientes, save)
    print("OPA")
    dfDesfechos = ReadDesfechos(filedesdechos, save)
    dfExames    = ReadExames(fileexames, save)
    print(dfPacientes)
    return dfPacientes, dfDesfechos, dfExames

def JoinPRExames(dfPR, dfExames):
    df = dfPR.set_index(['ID_PACIENTE', 'ID_ATENDIMENTO']).join(dfExames.set_index(['ID_PACIENTE', 'ID_ATENDIMENTO']))
    df = df.dropna()
    df.to_csv('pr_exames.csv', index=True, sep=";", encoding='utf-8')
    print(df.describe(include='all', percentiles=[.25, .5, .75]))
    return df

def CalcExames():  
    exames  = pandas.read_csv("pr_exames.csv",delimiter=";" )
    dfExames = pandas.DataFrame(exames)

    dfNExames = dfExames.groupby(['DE_EXAME','DE_ANALITO']).size().reset_index(name='counts')
    dfNExames.to_csv('numeroprexames.csv',  index=True, sep=";")
    df1 = pandas.DataFrame()
    for row in dfNExames.iterrows():
        
        # Não calcula exames com menos de 10 registros
        if row[1].counts < 10:
            print("Não: " + row[1].DE_EXAME + " - " + row[1].DE_ANALITO +" Counts: " + str(row[1].counts))
            continue

        de_exame   = row[1].DE_EXAME
        de_analito = row[1].DE_ANALITO 
        field = row[1].DE_EXAME + "__ANALITO__" + row[1].DE_ANALITO 
        filename = "exames2\\" + slugify(field) + ".csv"
        df = dfExames.query('DE_EXAME.str.startswith(@de_exame) and DE_ANALITO.str.startswith(@de_analito)')
        df.to_csv(filename, index=False, sep=";", encoding='utf-8') 
        df = df.filter(items=['DE_RESULTADO'])  
        df['DE_RESULTADO'] = df['DE_RESULTADO'].astype(float, errors="ignore")
        df = df.rename(columns={'DE_RESULTADO': slugify(field)})
        
        a = df.describe(include='all',  percentiles=[.25, .5, .75]).to_numpy().flatten().tolist() #.flatten()
        if( np.size(a) <= 4):
            continue

        iqr  = (a[6] - a[4]) * 1.5
        inf  = a[4] - iqr
        sup  = a[6] + iqr
        a.append(inf)
        a.append(sup)
        row = pandas.DataFrame([a], index=[slugify(field)], columns=['count', 'mean','std','min','p25', 'p50', 'p75', 'max', 'inf', 'sup'])
        df1 = df1.append(row)
        
        GenGraphs(field, df)

    df1.to_csv('exames_resumo.csv', index=True, sep=";", encoding='utf-8')
    return df1
    
def GenGraphs(field, df):
    fig = plt.figure(figsize = (8,8))
    ax1 = fig.gca()
    df.hist(ax=ax1)
    ax1.figure.savefig("exames2\\" + slugify(field) + "-histograma.png")
    plt.close(fig)

    fig2 = plt.figure(figsize = (8,8))
    ax2 = fig2.gca()
    ax2 = df.boxplot()
    fig2.savefig("exames2\\" + slugify(field) + "-boxplot.png")
    plt.close(fig2)


if __name__ == "__main__":
    main()